{
  _config+:: {
    inventree: {
      name: 'inventree',
      image: {
        repo: 'inventree/inventree',
        tag: 'stable',
      },
      config: {
        INVENTREE_EXT_VOLUME: '/home/me/inventree-data',
        INVENTREE_WEB_PORT: '8000', # Default web port for the InvenTree server

        INVENTREE_DEBUG: 'False',
        INVENTREE_LOG_LEVEL: 'WARNING',

        # InvenTree admin account details
        # Un-comment (and complete) these lines to auto-create an admin acount
        #INVENTREE_ADMIN_USER: '',
        #INVENTREE_ADMIN_PASSWORD: '',
        #INVENTREE_ADMIN_EMAIL: '',

        # Database configuration options
        # Note: The example setup is for a PostgreSQL database
        INVENTREE_DB_ENGINE: 'postgresql',
        INVENTREE_DB_NAME: 'inventree',
        INVENTREE_DB_HOST: 'inventree-db',
        INVENTREE_DB_PORT: '5432',

        # Database credentials - These must be configured before running
        # Uncomment the lines below, and change from the default values!
        #INVENTREE_DB_USER: 'pguser',
        #INVENTREE_DB_PASSWORD: 'pgpassword',

        # Redis cache setup (disabled by default)
        # Un-comment the following lines to enable Redis cache
        #INVENTREE_CACHE_HOST: 'inventree-cache',
        #INVENTREE_CACHE_PORT: '6379',

        # Enable custom plugins?
        INVENTREE_PLUGINS_ENABLED: 'False',
      },
    },
  },

  local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
  local deployment = k.apps.v1.deployment,
  local container = k.core.v1.container,
  local env = k.core.v1.envVar,
  local port = k.core.v1.containerPort,
  local service = k.core.v1.service,

  local util = import 'util/main.libsonnet',

  local withEnv(name, value) = container.withEnv(
    env.new(name=name, value=value)
  ),

  local cfg = self._config.inventree,

  inventree: {
    pvc:: util.volumeClaimTemplate.new(cfg.name, '25Gi'),


    configmap: k.core.v1.configMap.new(cfg.name, {
    }),

    deployment: deployment.new(
        name=cfg.name
        , replicas=1
        , containers=[
          container.new(
            'server'
            , cfg.image.repo + ':' + cfg.image.tag
          ) + container.withPorts([port.new('web', 8000)])
          // + container.withCommand(['invoke', 'worker'])
        ]
      ) + deployment.spec.withServiceName(self.service.metadata.name)
      + deployment.configVolumeMount(cfg.name, '/etc/garage', {})
      + deployment.pvcVolumeMount(self.pvc.name, '/home/inventree/data'),
  },
}
