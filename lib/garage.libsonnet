{
  _config+:: {
    garage: {
      name: 'garage',
      image: {
        repo: 'kube.cat/cocainefarm/garage',
        tag: 'latest',
      },

      kind: 'StatefulSet',

      region: 'garage',
      replication_mode: '1',
      rpc_secret: '',

      service_name: 'garage',
      namespace: 's3',

      s3_root_domain: 's3.vapor.systems',
      web_root_domain: 'web.vapor.systems',
    },
  },

  local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
  local statefulset = k.apps.v1.statefulSet,
  local daemonset = k.apps.v1.daemonSet,
  local container = k.core.v1.container,
  local env = k.core.v1.envVar,
  local port = k.core.v1.containerPort,
  local service = k.core.v1.service,

  local util = import 'util/main.libsonnet',

  local withEnv(name, value) = container.withEnv(
    env.new(name=name, value=value)
  ),

  local cfg = self._config.garage,

  garage: {
    pvc:: util.volumeClaimTemplate.new('data', '25Gi'),
    configmap: k.core.v1.configMap.new(cfg.name, {
      'garage.toml': |||
        metadata_dir = "/var/lib/garage/meta"
        data_dir = "/var/lib/garage/data"

        replication_mode = "%s"

        compression_level = 2

        rpc_bind_addr = "[::]:3901"
        rpc_secret = "%s"

        bootstrap_peers = []

        kubernetes_service_name = "%s"
        kubernetes_namespace = "%s"

        [s3_api]
        s3_region = "%s"
        api_bind_addr = "[::]:3900"
        root_domain = "%s"

        [s3_web]
        bind_addr = "[::]:3902"
        root_domain = "%s"
        index = "index.html"
      ||| % [cfg.replication_mode, cfg.rpc_secret, cfg.service_name, cfg.namespace, cfg.region, cfg.s3_root_domain, cfg.web_root_domain],
    }),
    deployment: if cfg.kind == 'StatefulSet' then
      statefulset.new(
        name=cfg.name
        , replicas=1
        , containers=[
          container.new(
            'garage'
            , cfg.image.repo + ':' + cfg.image.tag
          ) + container.withPorts([port.new('s3', 3900), port.new('web', 3902), port.new('rpc', 3901)])
          + container.withArgs(['/garage', '-c', '/etc/garage/garage.toml', 'server'])
          + container.withVolumeMounts(k.core.v1.volumeMount.new('data', '/var/lib/garage', false)),
        ]
      ) + statefulset.spec.withServiceName(self.service.metadata.name)
      + statefulset.configVolumeMount(cfg.name, '/etc/garage', {})
      + statefulset.spec.template.spec.withServiceAccountName(self.rbac.service_account.metadata.name)
      + statefulset.spec.withVolumeClaimTemplates([self.pvc])
    else daemonset.new(
           name=cfg.name,
           containers=[
             container.new(
               'garage'
               , cfg.image.repo + ':' + cfg.image.tag
             ) + container.withPorts([port.new('s3', 3900), port.new('web', 3902), port.new('rpc', 3901)])
             + container.withArgs(['/garage', '-c', '/etc/garage/garage.toml', 'server']),
           ]
         ) + daemonset.configVolumeMount(cfg.name, '/etc/garage', {})
         + daemonset.spec.template.spec.withServiceAccountName(self.rbac.service_account.metadata.name)
         + daemonset.hostVolumeMount('data', '/srv/garage', '/var/lib/garage', false, {})
         + daemonset.spec.template.metadata.withLabelsMixin({
           'app': cfg.name,
         }),
    service: k.util.serviceFor(self.deployment)
             + if cfg.kind == 'StatefulSet' then service.spec.withClusterIP('None') else {},

    rbac: {
      local name = '%s-garage' % cfg.name,
      local namespace = cfg.namespace,


      local clusterRole = k.rbac.v1.clusterRole,
      local clusterRoleBinding = k.rbac.v1.clusterRoleBinding,
      local subject = k.rbac.v1.subject,
      local serviceAccount = k.core.v1.serviceAccount,

      service_account:
        serviceAccount.new(name) +
        serviceAccount.mixin.metadata.withNamespace(namespace),

      cluster_role:
        clusterRole.new(name) +
        clusterRole.withRules([
          {
            apiGroups: ["apiextensions.k8s.io"],
            resources: ["customresourcedefinitions"],
            verbs: ["get", "list", "watch", "create", "update", "patch", "delete"],
          }
        ]),

      cluster_role_binding:
        clusterRoleBinding.new(name) +
        clusterRoleBinding.mixin.roleRef.withApiGroup('rbac.authorization.k8s.io') +
        clusterRoleBinding.mixin.roleRef.withKind('ClusterRole') +
        clusterRoleBinding.mixin.roleRef.withName(name) +
        clusterRoleBinding.withSubjects([
          subject.fromServiceAccount(self.service_account),
        ]),

      local role = k.rbac.v1.role,
      local roleBinding = k.rbac.v1.roleBinding,

      role:
        role.new(name) +
        role.mixin.metadata.withNamespace(namespace) +
        role.withRules([
          {
            apiGroups: ["deuxfleurs.fr"],
            resources: ["garagenodes"],
            verbs: ["get", "list", "watch", "create", "update", "patch", "delete"],
          }
        ]),

      role_binding:
        roleBinding.new(name) +
        roleBinding.mixin.metadata.withNamespace(namespace) +
        roleBinding.mixin.roleRef.withApiGroup('rbac.authorization.k8s.io') +
        roleBinding.mixin.roleRef.withKind('Role') +
        roleBinding.mixin.roleRef.withName(name) +
        roleBinding.withSubjects([
          subject.fromServiceAccount(self.service_account),
        ]),
    },
  },
}
