local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local util = import 'util/main.libsonnet';

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);

(util.inlineSpec('https://ettves.vapor.systems:6443', 'languagetool', null, null)) + {
  data: {
    local pvc = k.core.v1.persistentVolumeClaim,
    local pvcs = k.core.v1.persistentVolumeClaim,

    languagetool_models: pvc.new("languagetool-models")
                         + pvc.spec.withStorageClassName("ssd")
                         + pvc.spec.withAccessModes(["ReadWriteOnce"])
                         + pvc.spec.resources.withRequests({storage: "50Gi"}),
    languagetool: helm.template('languagetool', '../../charts/languagetool', {
      namespace: 'languagetool',
      values: {
        ngrams: {
          enabled: true,
        },
        word2vec: {
          enabled: true,
        },
        beolingus: {
          enabled: true,
        },
        fasttext: {
          enabled: true,
        },

        ingress: {
          host: "lang.cocaine.farm",
          path: "/",
          origins: [ "*" ],
          annotations: {
            "nginx.ingress.kubernetes.io/proxy-body-size": "12m"
          },
        },

        volume: |||
          persistentVolumeClaim:
            claimName: languagetool-models
        |||,
      },
    }),
  },
}
