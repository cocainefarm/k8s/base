local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local util = import 'util/main.libsonnet';

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);

(util.inlineSpec('https://ettves.vapor.systems:6443', 'inventree', null, null)) + {
  data: {
    inventree: helm.template('inventree', '../../charts/inventree', {
      namespace: 'inventree',
      values: {
        // https://github.com/truecharts/charts/blob/master/charts/stable/inventree/values.yaml
        image: {
          repository: "kube.cat/cocainefarm/inventree",
          tag: "0.9.2",
        },

        inventree: {
          credentials: {
            admin_mail: "info@cocaine.farm",
            admin_user: "admin",
            admin_password: "changeme",
          },
          general: {
            plugins_enabled: true,
          },
          mail: {
            backend: "django.core.mail.backends.smtp.EmailBackend",
            host: "mail.cocaine.farm",
            port: 587,
            username: "inventree",
            password: "rNJsUekpc9mOqrohyOj3MYCNq",
            tls: true,
            ssl: false,
            sender: "Do Not Reply",
          }
        },

        postgresql: {
          enabled: false,
          // existingSecret: "inventree-owner-user.postgresql",
          postgresqlPassword: "ptFTTPonrgBpaPNCaYzEY8OBvzE0iwkpFYP1CK3esVfwF6dLeaPVIw3PJqdHV7Xe",
        },

        redis: {
          existingSecret: "rediscreds",
        },
      },
    }) + {
      ingress: util.ingressFor(self.service_inventree, 'inventory.cocaine.farm', 'cocaine-farm-tls'),

      config_map_inventree_inventree_config+: {
        data+: {
          "INVENTREE_DB_HOST": "cocaine-postgres.postgres.svc",
          "INVENTREE_DB_NAME": "inventree",
          "INVENTREE_DB_PORT": "5432",
          "INVENTREE_DB_USER": "inventree_owner_user",
        },
      },

      persistent_volume_claim_inventree_data+: {
        spec+: {
          storageClassName: "ssd",
          resources: {
            requests: {
              storage: '10Gi',
            },
          },
        },
      },

      stateful_set_inventree_redis+: {
        spec+: {
          volumeClaimTemplates: [
            {
              "metadata": {
                "name": "data"
              },
              "spec": {
                "accessModes": [
                  "ReadWriteOnce"
                ],

                storageClassName: "ssd",
                "resources": {
                  "requests": {
                    "storage": "10Gi"
                  },
                },
              },
            },
          ],
        },
      },

      // deployment_inventree: self.deployment_inventree.mapContainers(function(c) c.withEnv(
      //   k.core.v1.envVar.withName("INVENTREE_DB_PASSWORD")
      //   + k.core.v1.envVar.valueFrom.secretKeyRef.withName("inventree-owner-user.postgresql")
      //   + k.core.v1.envVar.valueFrom.secretKeyRef.withKey("password"),
      // )),

      job_inventree_manifests: {},
      job_inventree_redis_manifests: {},
      cluster_role_inventree_manifests: {},
      cluster_role_inventree_redis_manifests: {},
      cluster_role_binding_inventree_manifests: {},
      cluster_role_binding_inventree_redis_manifests: {},
    },
  },
}
