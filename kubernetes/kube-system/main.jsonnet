local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local util = import 'util/main.libsonnet';

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);

(util.inlineSpec('https://10.10.0.1:6443', 'kube-system', null, null))
+ {
  _config:: self.data._config,
  data: {
    _config+:: {},

    'replicator': helm.template('replicator', '../../charts/kubernetes-replicator', {
      namespace: 'kube-system',
      values: {
        fullnameOverride: 'replicator',
      },
    }),

    zfs: helm.template('zfs-localpv', '../../charts/zfs-localpv', {
      namespace: 'kube-system',
      values: {
        crd: {
          enableInstall: true,
        },
        zfs: {
          bin: '/run/current-system/sw/bin/zfs',
        },
      },
    }),

    storageClass: {
      ssd: {
        apiVersion: 'storage.k8s.io/v1',
        kind: 'StorageClass',
        metadata: {
          name: 'ssd',
        },
        provisioner: 'zfs.csi.openebs.io',
        reclaimPolicy: 'Retain',
        volumeBindingMode: 'Immediate',
        allowVolumeExpansion: true,
        allowedTopologies: [
          {
            matchLabelExpressions: [
              {
                values: [
                  'ettves',
                ],
                key: 'kubernetes.io/hostname',
              },
            ],
          },
        ],
        parameters: {
          shared: 'yes',
          recordsize: '4k',
          poolname: 'rpool/data',
          fstype: 'zfs',
          dedup: 'off',
          compression: 'zstd',
        },
      },
      hdd: {
        apiVersion: 'storage.k8s.io/v1',
        kind: 'StorageClass',
        metadata: {
          name: 'hdd',
        },
        provisioner: 'zfs.csi.openebs.io',
        reclaimPolicy: 'Retain',
        volumeBindingMode: 'Immediate',
        allowVolumeExpansion: true,
        allowedTopologies: [
          {
            matchLabelExpressions: [
              {
                values: [
                  'phaenn',
                ],
                key: 'kubernetes.io/hostname',
              },
            ],
          },
        ],
        parameters: {
          shared: 'yes',
          recordsize: '4k',
          poolname: 'rpool/data',
          fstype: 'zfs',
          dedup: 'off',
          compression: 'zstd',
        },
      },
      shared: {
        apiVersion: 'storage.k8s.io/v1',
        kind: 'StorageClass',
        metadata: {
          name: 'shared',
        },
        provisioner: 'zfs.csi.openebs.io',
        reclaimPolicy: 'Retain',
        volumeBindingMode: 'WaitForFirstConsumer',
        allowVolumeExpansion: true,
        parameters: {
          shared: 'yes',
          recordsize: '4k',
          poolname: 'rpool/data',
          fstype: 'zfs',
          dedup: 'off',
          compression: 'zstd',
        },
      },
    },

    cilium: helm.template('cilium', '../../charts/cilium', {
      namespace: 'kube-system',
      values: {
        installIptablesRules: true,
        l7Proxy: false,
        localRedirectPolicy: true,
        hubble: {
          ui: {
            ingress: {
              tls: [
                {
                  hosts: [
                    'cocaine-farm-tls',
                  ],
                  secretName: 'cocaine-farm-tls',
                },
              ],
              hosts: [
                'hubble.cocaine.farm',
              ],
              path: '/',
              annotations: {
                'kubernetes.io/ingress.class': 'nginx',
              },
              enabled: true,
            },
            enabled: true,
          },
          enabled: true,
          listenAddress: ':4244',
          relay: {
            enabled: true,
          },
        },
        prometheus: {
          serviceMonitor: {
            enabled: false,
          },
          port: 9090,
          enabled: false,
        },
        ipam: {
          operator: {
            clusterPoolIPv4PodCIDRList: ['10.102.0.0/16'],
            clusterPoolIPv4MaskSize: '24',
            clusterPoolIPv6PodCIDRList: ['fd15:3d8c:d429:0102::/64'],
            clusterPoolIPv6MaskSize: '72',
          },
        },
        image: {
          repository: 'kube.cat/cocainefarm/cilium-dev',
          tag: 'latest',
          useDigest: false,
        },
        rollOutCiliumPods: true,
        egressMasqueradeInterfaces: 'eth0',
        enableCiliumEndpointSlice: true,
        bpf: {
          masquerade: false,
          lbExternalClusterIP: true,
        },
        ipv6: {
          enabled: true,
        },
        containerRuntime: {
          integration: 'none',
        },
        etcd: {
          managed: false,
          enabled: false,
        },
        egressGateway: {
          enabled: false,
        },
        nodePort: {
          enabled: true,
        },
        hostServices: {
          protocols: 'tcp,udp',
          enabled: true,
        },
        externalIPs: {
          enabled: true,
        },
        hostPort: {
          enabled: true,
        },
        loadBalancer: {
          mode: 'dsr',
          algorithm: 'maglev',
          enabled: true,
        },
        k8sServicePort: 6443,
        k8sServiceHost: '10.10.0.1',
        kubeProxyReplacement: 'strict',
        nativeRoutingCIDR: '10.0.0.0/8',
        endpointRoutes: {
          enabled: true,
        },
        tunnel: 'disabled',
        autoDirectNodeRoutes: true,
        operator: {
          enabled: true,
          replicas: 1,
        },
      },
    }),
  },
}
