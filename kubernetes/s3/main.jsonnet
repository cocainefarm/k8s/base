local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local util = import 'util/main.libsonnet';

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);

(util.inlineSpec('https://ettves.vapor.systems:6443', 's3', null, null))
+ {
  spec+: {
    injectLabels: true,
  },
} + {
  data: {
    storage: (import 'garage.libsonnet') + {
      _config+:: {
        garage+: {
          name: 'storage',
          region: 'storage',
          replication_mode: '1',
          rpc_secret: '76329df6a41d4c4b009c1fbdd07137d13bab6bcceba807bcf03e8112a614e8b0',
          s3_root_domain: 's3.storage.vapor.systems',
          web_root_domain: 'web.storage.vapor.systems',

          service_name: 'storage',
          namespace: 's3',
        },
      },
      garage+: {
        pvc+:: util.volumeClaimTemplate.withStorageClass('hdd')
               + util.volumeClaimTemplate.withStorageRequests('250Gi'),
      },
    },
    cache: (import 'garage.libsonnet') + {
      _config+:: {
        garage+: {
          name: 'cache',
          region: 'cache',
          replication_mode: '1',
          rpc_secret: '76329df6a41d4c4b009c1fbdd07137d13bab6bcceba807bcf03e8112a614e8b0',
          s3_root_domain: 's3.cache.vapor.systems',
          web_root_domain: 'web.cache.vapor.systems',

          service_name: 'cache',
          namespace: 's3',
        },
      },
      garage+: {
        pvc+:: util.volumeClaimTemplate.withStorageClass('ssd')
               + util.volumeClaimTemplate.withStorageRequests('50Gi'),

        serviceIngress: util.serviceFor(self.deployment) + k.core.v1.service.metadata.withName('cache-ingress'),
      },
    },
    cdn: (import 'garage.libsonnet') + {
      _config+:: {
        garage+: {
          name: 'cdn',
          kind: 'DaemonSet',

          region: 'cdn',
          replication_mode: '3',
          rpc_secret: '37e1edc5a5eefb8901ca314bcfbd21cb803fbfb0a780b80a547fddf641284503',
          s3_root_domain: 's3.vapor.systems',
          web_root_domain: 'web.vapor.systems',

          service_name: 'cdn',
          namespace: 's3',
        },
      },
      garage+: {
        deployment+: k.apps.v1.daemonSet.spec.template.spec.withTolerations([
          k.core.v1.toleration.withKey('role')
          + k.core.v1.toleration.withValue('ns')
          + k.core.v1.toleration.withOperator('Equal')
          + k.core.v1.toleration.withEffect('NoSchedule'),
        ]) + k.apps.v1.daemonSet.spec.template.spec.withNodeSelector({
          'kubernetes.io/role': 'ns',
        }),
        // service+: k.core.v1.service.metadata.withAnnotationsMixin({
        //   'service.kubernetes.io/topology-aware-hints': 'auto',
        // }),
        service+: k.core.v1.service.spec.withPorts([
          {
            targetPort: 3900,
            protocol: 'TCP',
            port: 3900,
            name: 's3',
          },
          {
            targetPort: 3902,
            protocol: 'TCP',
            port: 3902,
            name: 'web',
          },
          {
            targetPort: 3901,
            protocol: 'TCP',
            port: 3901,
            name: 'rpc',
          },
        ]),
      },

      redirectPolicy: {
        apiVersion: 'cilium.io/v2',
        kind: 'CiliumLocalRedirectPolicy',
        metadata: {
          name: 'cdn',
          namespace: 's3',
        },
        spec: {
          redirectFrontend: {
            serviceMatcher: {
              serviceName: 'cdn',
              namespace: 's3',
            },
          },
          redirectBackend: {
            localEndpointSelector: {
              matchLabels: {
                app: 'cdn',
              },
            },
            toPorts: [
              {
                port: '3900',
                name: 's3',
                protocol: 'TCP',
              },
              {
                port: '3901',
                name: 'rpc',
                protocol: 'TCP',
              },
              {
                port: '3902',
                name: 'web',
                protocol: 'TCP',
              },
            ],
          },
        },
      },

      ingress: {
        local ingress = k.networking.v1.ingress,
        local ingressRule = k.networking.v1.ingressRule,
        local ingressTLS = k.networking.v1.ingressTLS,
        local httpIngressPath = k.networking.v1.httpIngressPath,
        local service = k.core.v1.service,

        cert(name, secret, domains):: {
          apiVersion: 'cert-manager.io/v1',
          kind: 'Certificate',
          metadata: {
            name: name,
          },
          spec: {
            issuerRef: {
              group: 'cert-manager.io',
              kind: 'ClusterIssuer',
              name: 'cocainedns',
            },
            dnsNames: domains,
            secretName: secret,
          },
        },

        ingress(name, domain, secret, acme=true):: ingress.new(name) +
                                        (if acme then ingress.metadata.withAnnotations({
                                          'kubernetes.io/tls-acme': 'true',
                                        }) else {}) +
                                        ingress.mixin.spec.withIngressClassName('cdn') +
                                        ingress.mixin.spec.withRules(
                                          [
                                            ingressRule.withHost(domain) +
                                            ingressRule.mixin.http.withPaths(
                                              httpIngressPath.withPath('/') +
                                              httpIngressPath.withPathType('Prefix') +
                                              httpIngressPath.backend.service.withName('cdn') +
                                              httpIngressPath.backend.service.port.withName('web')
                                            ),
                                          ]
                                        ) +
                                        ingress.mixin.spec.withTls(
                                          ingressTLS.withHosts(domain) +
                                          ingressTLS.withSecretName(secret)
                                        ),

        gnulag: self.ingress('gnulag-net', 'gnulag.net', 'gnulag-net-tls'),

        lmrIngress: self.ingress('linuxmasterrace-org', 'linuxmasterrace.org', 'linuxmasterrace-org-tls', acme=false),
        lmrCert: self.cert('linuxmasterrace-org', 'linuxmasterrace-org-tls', ['linuxmasterrace.org', '*.linuxmasterrace.org']),

        s3: ingress.new('cdn-s3') +
            ingress.metadata.withAnnotations({
              'nginx.ingress.kubernetes.io/proxy-body-size': '9999999m',
               'nginx.ingress.kubernetes.io/service-upstream': 'true',
            }) +
            ingress.mixin.spec.withIngressClassName('cdn') +
            ingress.mixin.spec.withRules(
              ingressRule.withHost('s3.vapor.systems') +
              ingressRule.mixin.http.withPaths(
                httpIngressPath.withPath('/') +
                httpIngressPath.withPathType('Prefix') +
                httpIngressPath.backend.service.withName('cdn') +
                httpIngressPath.backend.service.port.withName('s3')
              )
            ) +
            ingress.mixin.spec.withTls(
              ingressTLS.withHosts('s3.vapor.systems') +
              ingressTLS.withSecretName('vapor-systems-tls')
            ),

        web: ingress.new('cdn-web') +
             ingress.metadata.withAnnotations({
               'nginx.ingress.kubernetes.io/server-alias': '~^.*\\.web\\.vapor\\.systems$',
               'nginx.ingress.kubernetes.io/service-upstream': 'true',
             }) +
             ingress.mixin.spec.withIngressClassName('cdn') +
             ingress.mixin.spec.withRules(
               [
                 ingressRule.withHost('web.vapor.systems') +
                 ingressRule.mixin.http.withPaths(
                   httpIngressPath.withPath('/') +
                   httpIngressPath.withPathType('Prefix') +
                   httpIngressPath.backend.service.withName('cdn') +
                   httpIngressPath.backend.service.port.withName('web')
                 ),
               ]
             ) +
             ingress.mixin.spec.withTls(
               ingressTLS.withHosts('web.vapor.systems') +
               ingressTLS.withSecretName('vapor-systems-tls')
             ),


        cacheCert: self.cert('cache-vapor-systems', 'cache-vapor-systems-tls', ['cache.vapor.systems', '*.cache.vapor.systems']),
        cacheWeb: ingress.new('cache-web') +
             ingress.metadata.withAnnotations({
               'nginx.ingress.kubernetes.io/server-alias': '~^.*\\.cache\\.vapor\\.systems$',
               'nginx.ingress.kubernetes.io/service-upstream': 'true',
             }) +
             ingress.mixin.spec.withIngressClassName('nginx') +
             ingress.mixin.spec.withRules(
               [
                 ingressRule.withHost('nix.cache.vapor.systems') +
                 ingressRule.mixin.http.withPaths(
                   httpIngressPath.withPath('/') +
                   httpIngressPath.withPathType('Prefix') +
                   httpIngressPath.backend.service.withName('cache-ingress') +
                   httpIngressPath.backend.service.port.withName('web')
                 ),
               ]
             ) +
             ingress.mixin.spec.withTls(
               ingressTLS.withHosts('cache.vapor.systems') +
               ingressTLS.withSecretName('cache-vapor-systems-tls')
             ),
        cacheS3: ingress.new('cache-s3') +
             ingress.metadata.withAnnotations({
               'nginx.ingress.kubernetes.io/proxy-body-size': '0m',
               'nginx.ingress.kubernetes.io/service-upstream': 'true',
             }) +
             ingress.mixin.spec.withIngressClassName('nginx') +
             ingress.mixin.spec.withRules(
               [
                 ingressRule.withHost('cache.vapor.systems') +
                 ingressRule.mixin.http.withPaths(
                   httpIngressPath.withPath('/') +
                   httpIngressPath.withPathType('Prefix') +
                   httpIngressPath.backend.service.withName('cache-ingress') +
                   httpIngressPath.backend.service.port.withName('s3')
                 ),
               ]
             ) +
             ingress.mixin.spec.withTls(
               ingressTLS.withHosts('cache.vapor.systems') +
               ingressTLS.withSecretName('cache-vapor-systems-tls')
             ),
      },
    },
  },
}
