local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local util = import 'util/main.libsonnet';

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);

(util.inlineSpec('https://ettves.vapor.systems:6443', 'ingress', null, null))
+ {
  spec+: {
    injectLabels: true,
  },
} + {
  data: {
    'cert-manager': helm.template('cert-manager', '../../charts/cert-manager', {
      namespace: 'ingress',
      values: {
        installCRDs: true,
        // extraArgs: [
        //   '--dns01-recursive-nameservers=178.63.224.15:53',
        //   '--dns01-recursive-nameservers-only=true',
        // ],
        ingressShim: {
          defaultIssuerGroup: 'cert-manager.io',
          defaultIssuerKind: 'ClusterIssuer',
          defaultIssuerName: 'cocainedns',
        },
      },
    }),
    'cert-manager-webhook-pdns': helm.template('cert-manager', '../../charts/cert-manager-webhook-pdns', {
      namespace: 'ingress',
      values: {
        fullnameOverride: 'cert-manager-webhook-pdns',
        certManager: {
          serviceAccountName: 'cert-manager',
          namespace: 'ingress',
        },
      },
    }),
    pdnsApiKey: std.native('parseYaml')(importstr '../../secrets/kubernetes/ingress/pdns-api-key.yaml'),
    clusterIssuer: {
      apiVersion: 'cert-manager.io/v1',
      kind: 'ClusterIssuer',
      metadata: {
        name: 'cocainedns',
      },
      spec: {
        acme: {
          solvers: [
            {
              dns01: {
                webhook: {
                  solverName: 'pdns',
                  groupName: 'acme.zacharyseguin.ca',
                  config: {
                    host: 'http://pdns-primary.ns.svc:8081',
                    apiKeySecretRef: {
                      name: 'pdns-api-key',
                      key: 'key',
                    },
                  },
                },
              },
            },
          ],
          server: 'https://acme-v02.api.letsencrypt.org/directory',
          privateKeySecretRef: {
            name: 'prod-issuer-account-key',
          },
          preferredChain: '',
          email: 'audron@cocaine.farm',
        },
      },
    },
    cocainefarmCert: {
      apiVersion: 'cert-manager.io/v1',
      kind: 'Certificate',
      metadata: {
        name: 'cocaine-farm',
      },
      spec: {
        issuerRef: {
          group: 'cert-manager.io',
          kind: 'ClusterIssuer',
          name: 'cocainedns',
        },
        dnsNames: [
          'cocaine.farm',
          '*.cocaine.farm',
          '*.media.cocaine.farm',
        ],
        secretName: 'cocaine-farm-tls',
      },
    },
    vaporSystemsCert: {
      apiVersion: 'cert-manager.io/v1',
      kind: 'Certificate',
      metadata: {
        name: 'vapor-systems',
      },
      spec: {
        issuerRef: {
          group: 'cert-manager.io',
          kind: 'ClusterIssuer',
          name: 'cocainedns',
        },
        dnsNames: [
          'vapor.systems',
          '*.vapor.systems',
        ],
        secretName: 'vapor-systems-tls',
      },
    },
    'ingress-nginx': helm.template('ingress-nginx', '../../charts/ingress-nginx', {
      namespace: 'ingress',
      values: {
        controller: {
          replicaCount: 2,
          metrics: {
            serviceMonitor: {
              enabled: false,
            },
            service: {
              annotations: {
                'prometheus.io/port': '10254',
                'prometheus.io/scrape': 'true',
              },
            },
            enabled: true,
          },
          ingressClassResource: {
            default: true,
            enabled: true,
            name: 'nginx',
          },
          watchIngressWithoutClass: false,
          stats: {
            enabled: true,
          },
          service: {
            enableHttps: true,
            enableHttp: true,
            loadBalancerSourceRanges: [],
            loadBalancerIP: null,
            externalIPs: [
              '195.201.245.25',
              // '142.132.159.202',
              '178.63.224.10',
              '178.63.224.11',
              '178.63.224.12',
              '178.63.224.13',
              '178.63.224.14',
            ],
            type: 'ClusterIP',
          },
          nodeSelector: {
            'kubernetes.io/hostname': 'ettves',
          },
          headers: {
          },
          config: {
            'ssl-protocols': 'TLSv1.2 TLSv1.3',
          },
          extraArgs: {
            'default-ssl-certificate': 'ingress/cocaine-farm-tls',
          },
        },
      },
    }),
    'ingress-cdn': helm.template('ingress-cdn', '../../charts/ingress-nginx', {
      namespace: 'ingress',
      values: {
        controller: {
          replicaCount: 2,
          kind: "DaemonSet",
          metrics: {
            serviceMonitor: {
              enabled: false,
            },
            service: {
              annotations: {
                'prometheus.io/port': '10254',
                'prometheus.io/scrape': 'true',
              },
            },
            enabled: true,
          },
          ingressClassResource: {
            default: false,
            enabled: true,
            name: 'cdn',
          },
          watchIngressWithoutClass: false,
          stats: {
            enabled: true,
          },
          service: {
            annotations: {
              'metallb.universe.tf/loadBalancerIPs': '209.250.238.254,2a0f:9400:8020::200',
              'metallb.universe.tf/address-pool': 'bgp',
            },
            ipFamilyPolicy: "RequireDualStack",
            ipFamilies: ["IPv4", "IPv6"],
            enableHttps: true,
            enableHttp: true,
            loadBalancerSourceRanges: [],
            loadBalancerIP: null,
            externalTrafficPolicy: 'Local',
            type: 'LoadBalancer',
          },
          nodeSelector: {
            'kubernetes.io/role': 'ns',
          },
          tolerations: [
            {
              key: "role",
              value: "ns",
              operator: "Equal",
              effect: "NoSchedule",
            }
          ],
          headers: {
          },
          config: {
            'ssl-protocols': 'TLSv1.2 TLSv1.3',
          },
          extraArgs: {
            'default-ssl-certificate': 'ingress/cocaine-farm-tls',
          },
        },
      },
    }),
    metallb: helm.template('metallb', '../../charts/metallb', {
      namespace: 'ingress',
      values: {
        controller: {
          // image: {
          //   repository: 'quay.io/metallb/controller',
          //   tag: 'main',
          //   pullPolicy: 'Always',
          // },
        },
        configInline: {
          peers: [
            {
              'my-asn': 64716,
              'peer-asn': 64515,
              'peer-address': '2001:19f0:ffff::1',
              password: importstr "../../secrets/ns/bgp",
              'ebgp-multihop': true,
            },
            {
              'my-asn': 64716,
              'peer-asn': 64515,
              'peer-address': '169.254.169.254',
              password: importstr "../../secrets/ns/bgp",
              'ebgp-multihop': true,
            },
          ],

          'bgp-communities': {
            'no-advertise': '20473:6000',
          },
          'address-pools': [
            {
              addresses: [
                '2a0f:9400:8020::/48',
                '209.250.238.254/32',
              ],
              protocol: 'bgp',
              name: 'bgp',
              'bgp-advertisements': [
                {
                  'aggregation-length-v6': 128,
                  localpref: 100,
                  communities: [
                    'no-advertise',
                  ],
                },
                {
                  'aggregation-length-v6': 48,
                },
              ],
            },
            {
              addresses: [
                '217.163.29.14/32',
              ],
              protocol: 'bgp',
              name: 'ns',
              'auto-assign': false,
            },
          ],
        },
        speaker: {
          // image: {
          //   repository: 'quay.io/metallb/speaker',
          //   tag: 'main',
          //   pullPolicy: 'Always',
          // },
          frr: {
            enabled: true,
          },
          nodeSelector: {
            'kubernetes.io/role': 'ns',
          },
          tolerations: [
            {
              key: 'role',
              value: 'ns',
              effect: 'NoSchedule',
              operator: 'Equal',
            },
          ],
        },
      },
    }),
  },
}
