local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local util = import 'util/main.libsonnet';

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);

(util.inlineSpec('https://10.10.0.1:6443', 'gitlab', null, null))
+ {
  spec+: {
    injectLabels: true,
  },
} + {
  data: {
    local deployment = k.apps.v1.deployment,
    local statefulset = k.apps.v1.statefulSet,
    local container = k.core.v1.container,
    local configMap = k.core.v1.configMap,
    local secret = k.core.v1.secret,
    local port = k.core.v1.containerPort,
    local service = k.core.v1.service,
    local servicePort = k.core.v1.servicePort,

    local envVar = k.core.v1.envVar,

    local agentToken = std.stripChars(importstr '../../secrets/kubernetes/gitlab/agent-token', '\n'),

    'gitlab-runner': helm.template('gitlab-runner', '../../charts/gitlab-runner', {
      namespace: 'gitlab',
      values: {
        runners: {
          config: importstr 'gitlab-runner.toml',
          cache: {
            secretName: 'gitlab-runner-cache',
          },
          name: 'vapor.systems',
          runUntagged: true,
          tags: 'vapor.systems,k8s,amd64',
          privileged: false,
          image: 'alpine:latest',
        },
        rbac: {
          clusterWideAccess: false,
          create: true,
        },
        metrics: {
          enabled: true,
        },
        checkInterval: 15,
        concurrent: 10,
        unregisterRunners: true,
        runnerRegistrationToken: 'GR1348941QYtc2s6zjqPZNEhNWxkd',
        gitlabUrl: 'https://gitlab.com/',
      },
    }),


    agent: {
      secret: secret.new('gitlab-agent-token', {
        token: std.base64(agentToken),
      }),
      deployment: deployment.new(
                    replicas=1
                    , name='gitlab-agent'
                    , containers=[
                      container.new(
                        'agent'
                        , 'registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/agentk:stable'
                      ) + container.withPorts([port.newNamed(8080, 'http')])
                      + container.withArgs([
                        '--token-file=/config/token',
                        '--kas-address=wss://kas.gitlab.com',
                      ])
                      + container.withEnv([
                        envVar.withName('POD_NAMESPACE')
                        + envVar.valueFrom.fieldRef.withFieldPath('metadata.namespace'),
                        envVar.withName('POD_NAME')
                        + envVar.valueFrom.fieldRef.withFieldPath('metadata.name'),
                      ])
                      + container.withImagePullPolicy('Always')
                      + util.httpProbes('http', '/', 20, 15, 10, 5)
                      + container.livenessProbe.httpGet.withPath('/liveness')
                      + container.readinessProbe.httpGet.withPath('/readiness'),
                    ]
                  ) + k.util.secretVolumeMount('gitlab-agent-token', '/config', defaultMode=292)
                  + deployment.spec.template.spec.withServiceAccountName('gitlab-agent')
                  + deployment.spec.template.metadata.withAnnotations({
                    'prometheus.io/path': '/metrics',
                    'prometheus.io/port': '8080',
                    'prometheus.io/scrape': 'true',
                  })
      ,
      rbac: {
        local clusterRole = k.rbac.v1.clusterRole,
        local clusterRoleBinding = k.rbac.v1.clusterRoleBinding,
        local subject = k.rbac.v1.subject,
        local serviceAccount = k.core.v1.serviceAccount,

        service_account: serviceAccount.new('gitlab-agent'),

        cluster_role_binding:
          clusterRoleBinding.new() +
          clusterRoleBinding.mixin.metadata.withName('gitlab-agent-cluster-admin') +
          clusterRoleBinding.mixin.roleRef.withApiGroup('rbac.authorization.k8s.io') +
          clusterRoleBinding.mixin.roleRef.withKind('ClusterRole') +
          clusterRoleBinding.mixin.roleRef.withName('cluster-admin') +
          clusterRoleBinding.withSubjects([
            subject.new() +
            subject.withKind('ServiceAccount') +
            subject.withName(self.service_account.metadata.name) +
            subject.withNamespace('gitlab'),
          ]),
      }
    },
  },
}
