local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local util = import 'util/main.libsonnet';

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);

(util.inlineSpec('https://ettves.vapor.systems:6443', 'postgres', null, null)) + {
  data: {
    postgres: {
      apiVersion: 'acid.zalan.do/v1',
      kind: 'postgresql',
      metadata: {
        namespace: 'postgres',
        name: 'cocaine-postgres',
      },
      spec: {
        teamId: 'cocaine',
        postgresql: {
          version: '14',
        },
        volume: {
          size: '50Gi',
          storageClass: 'shared',
        },
        resources+: {
          requests: {
            cpu: '100m',
            memory: '100Mi'
          },
          limits: {
            cpu: '16',
            memory: '32Gi'
          },
        },
        numberOfInstances: 2,
        users: {
          superuser: [
            'superuser',
            'createdb',
          ],
        },
        patroni: {
          pg_hba: [
            "local   all             all                                   trust",
            "hostssl all             +zalandos    127.0.0.1/32       pam",
            "host    all             all                127.0.0.1/32       md5",
            "hostssl all             +zalandos    ::1/128            pam",
            "host    all             all                ::1/128            md5",
            "local   replication     standby                    trust",
            "hostssl replication     standby all                md5",
            "host all           all                all                md5",
            "hostssl all             +zalandos    all                pam",
            "hostssl all             all                all                md5",
          ],
        },
        preparedDatabases: {
          quassel: {
            defaultUsers: true,
            secretNamespace: 'quassel',
            schemas: {
              public: {
                defaultUsers: false,
              }
            },
          },
          irl_map_dev: {
            defaultUsers: true,
            secretNamespace: 'irl-map-backend-dev',
            schemas: {
              public: {
                defaultUsers: false,
              }
            },
          },
          inventree: {
            defaultUsers: true,
            secretNamespace: 'inventree',
            schemas: {
              public: {
                defaultUsers: false,
              }
            },
          },
          authentik: {
            defaultUsers: true,
            secretNamespace: 'auth',
            schemas: {
              public: {
                defaultUsers: false,
              }
            },
          },
          stracker: {
            defaultUsers: true,
            secretNamespace: 'c0re',
            schemas: {
              public: {
                defaultUsers: false,
              }
            },
          },
        },
      },
    },
    'postgres-operator': helm.template('postgres-operator', '../../charts/postgres-operator', {
      namespace: 'postgres',
      values: {
        configMajorVersionUpgrade: {
          major_version_upgrade_mode: 'manual',
        },
        configKubernetes: {
          cluster_domain: 'kube.vapor.systems',
          enable_pod_antiaffinity: true,
          enable_cross_namespace_secret: true,
          secret_name_template: '{username}.postgresql'
        },
      },
    }),
    'postgres-operator-ui': helm.template('postgres-operator-ui', '../../charts/postgres-operator-ui', {
      namespace: 'postgres',
      values: {
        envs: {
          targetNamespace: 'postgres',
        },
        ingress: {
          enabled: false,
          // annotations: {
          //   'nginx.ingress.kubernetes.io/auth-response-headers': 'x-auth-request-user,x-auth-request-email,x-auth-request-access-token',
          //   'nginx.ingress.kubernetes.io/auth-signin': 'https://oauth.cocaine.farm/oauth2/start?rd=$scheme://$best_http_host$request_uri',
          //   'nginx.ingress.kubernetes.io/auth-url': 'https://oauth.cocaine.farm/oauth2/auth',
          // },
          ingressClassName: 'nginx',
          hosts: [{
            host: 'postgres.vapor.systems',
            paths: ['/'],
          }],
          tls: [{
            secretName: 'vapor-systems-tls',
            hosts: ['postgres.vapor.systems'],
          }],
        },
      },
    }),
  },
}
