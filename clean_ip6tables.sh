#!/usr/bin/env bash

TYPE=$1

${TYPE}tables -t filter -D FORWARD -m comment --comment "cilium-feeder: CILIUM_FORWARD" -j OLD_CILIUM_FORWARD
${TYPE}tables -t filter -D OUTPUT -m comment --comment "cilium-feeder: CILIUM_OUTPUT" -j OLD_CILIUM_OUTPUT
${TYPE}tables -t filter -D FORWARD -m comment --comment "cilium-feeder: CILIUM_FORWARD" -j CILIUM_FORWARD
${TYPE}tables -t filter -D OUTPUT -m comment --comment "cilium-feeder: CILIUM_OUTPUT" -j CILIUM_OUTPUT

${TYPE}tables -t raw -D PREROUTING -m comment --comment "cilium-feeder: CILIUM_PRE_raw" -j OLD_CILIUM_PRE_raw
${TYPE}tables -t raw -D OUTPUT -m comment --comment "cilium-feeder: CILIUM_OUTPUT_raw" -j OLD_CILIUM_OUTPUT_raw
${TYPE}tables -t raw -D PREROUTING -m comment --comment "cilium-feeder: CILIUM_PRE_raw" -j CILIUM_PRE_raw
${TYPE}tables -t raw -D OUTPUT -m comment --comment "cilium-feeder: CILIUM_OUTPUT_raw" -j CILIUM_OUTPUT_raw

${TYPE}tables -t mangle -D PREROUTING -m comment --comment "cilium-feeder: CILIUM_PRE_mangle" -j CILIUM_PRE_mangle
${TYPE}tables -t mangle -D PREROUTING -m comment --comment "cilium-feeder: CILIUM_PRE_mangle" -j OLD_CILIUM_PRE_mangle

${TYPE}tables -t nat -D POSTROUTING -m comment --comment "cilium-feeder: CILIUM_POST_nat" -j CILIUM_POST_nat
${TYPE}tables -t nat -D POSTROUTING -m comment --comment "cilium-feeder: CILIUM_POST_nat" -j OLD_CILIUM_POST_nat

#

nft_del() {
    nft flush chain ${TYPE} raw ${OLD}CILIUM_PRE_raw
    nft flush chain ${TYPE} raw ${OLD}CILIUM_OUTPUT_raw

    nft flush chain ${TYPE} mangle ${OLD}CILIUM_PRE_mangle

    nft flush chain ${TYPE} filter ${OLD}CILIUM_INPUT
    nft flush chain ${TYPE} filter ${OLD}CILIUM_OUTPUT

    nft flush chain ${TYPE} nat ${OLD}CILIUM_POST_nat

    #

    nft delete chain ${TYPE} raw ${OLD}CILIUM_PRE_raw
    nft delete chain ${TYPE} raw ${OLD}CILIUM_OUTPUT_raw

    nft delete chain ${TYPE} mangle ${OLD}CILIUM_PRE_mangle

    nft delete chain ${TYPE} filter ${OLD}CILIUM_INPUT
    nft delete chain ${TYPE} filter ${OLD}CILIUM_OUTPUT

    nft delete chain ${TYPE} nat ${OLD}CILIUM_POST_nat
}

nft_del
OLD="OLD_"
nft_del
